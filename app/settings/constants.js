import { Map } from 'immutable';

export default Map({
  PROJECT_DATA: {
    repository: 'https://github.com/RealOrangeOne/Sphere',
    CI_Badge: 'https://circleci.com/gh/RealOrangeOne/Sphere.svg?style=svg'
  },
  STORAGE_KEY: 'CIRCLE_TOKEN'
});
