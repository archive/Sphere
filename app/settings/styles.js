import { Platform } from 'react-native';
import { Map } from 'immutable';

export default Map({
  CIRCLE_GREEN: '#292',
  CIRCLE_NAVBAR_BG: '#3D3D3D',
  CIRCLE_NAVBAR_TEXT: '#E0E0E0',
  CIRCLE_BG: '#F5F5F5',
  CIRCLE_TEXT: '#212121',
  CIRCLE_ITEM_BG: '#fff',
  CIRCLE_ITEM_BORDER: '#E5E5E5',

  CIRCLE_TEST_COLOURS: Map({
    SUCCESS: '#42C88A',
    FAILED: '#ED5C5C',
    FIXED: '#42C88A',
    RUNNING: '#66D3E4',
    RETRIED: '#898989'
  }),

  TITLE_FONT_SIZE: 24,
  NAVBAR_HEIGHT: (Platform.OS === 'ios' ? 64 : 56),
  BANNER_HEIGHT: (Platform.OS === 'ios' ? 20 : 0),
});
