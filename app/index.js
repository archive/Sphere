import React from 'react-native';

import RouteMaster from './components/routes/RouteMaster';
import RouteMapper from './components/navigation/RouteMapper';
import GlobalStyles from './settings/styles';
import BusyIndicator from 'react-native-busy-indicator';
import token from './api/token';
import { checkToken } from './api/CircleCI';
import NavigationBar from './components/navigation/NavigationBar';

import 'moment-duration-format';

const {
  Navigator,
  StyleSheet,
  View,
  Alert
} = React;

const styles = StyleSheet.create({
  navbar: {
    backgroundColor: GlobalStyles.get('CIRCLE_NAVBAR_BG'),
    flexDirection:'row',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    marginTop: GlobalStyles.get('NAVBAR_HEIGHT')
  }
});

const LOGGED_IN_ROUTE = RouteMaster.get('HOME');
const LOGIN_ROUTE = RouteMaster.get('LOGIN');


export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      initialRoute: false
    };
  }

  renderScene(route, nav) {
    const Component = route.component;
    const props = route.props || {};
    const navbarStyles = {
      marginTop: (route.hideNavbar ? GlobalStyles.get('BANNER_HEIGHT') : GlobalStyles.get('NAVBAR_HEIGHT'))
    };
    return (
      <View style={[styles.container, navbarStyles]}>
        <Component
          nav={nav}
          currentRoute={route}
          {...props} />
      </View>
    );
  }

  warnInvalidToken() {
    Alert.alert(
      'Token Invalidated',
      'The token you used previously is now invalid. Please generate a new one in your account settings.',
      [
        { text: 'ok' }
      ]
    );
  }

  componentWillMount() {
    token.get().then(function (CIToken) {
      if (!CIToken) {
        this.setState({
          initialRoute: LOGIN_ROUTE
        });
        return;
      }
      checkToken(CIToken).then(function (isValid) {
        this.setState({
          initialRoute: isValid ? LOGGED_IN_ROUTE : LOGIN_ROUTE
        });
        if (!isValid) {
          this.warnInvalidToken();
        }
      }.bind(this));
    }.bind(this));
  }

  render() {
    if (!this.state.initialRoute) {
      return null;  // Replace with a splash screen?
    }
    return (
      <View style={{ flex: 1 }}>
        <Navigator
          renderScene={this.renderScene}
          initialRoute={this.state.initialRoute}
          navigationBar={
            <NavigationBar
              style={styles.navbar}
              routeMapper={RouteMapper} />
          }
          configureScene={(route) => {
            if (route.sceneConfig) {
              return route.sceneConfig;
            }
            return Navigator.SceneConfigs.PushFromRight;
          }}
        />
        <BusyIndicator />
      </View>
    );
  }
};
