import token from './token';

export default async function request(url, method = 'GET', data = {}, CIToken) {
  CIToken = CIToken || await token.get();
  const fullURL = url.query({ 'circle-token': CIToken }).toString();
  let fetchParams = {
    method,
    headers: {
      'Accept': 'application/json',
    },
  };
  if (method !== 'GET' && method !== 'HEAD') {
    fetchParams.body = JSON.stringify(data);
    fetchParams.headers['Content-Type'] = 'application/json';
  }
  return await fetch(fullURL, fetchParams);
}
