import { Map } from 'immutable';
import UrlAssembler from 'url-assembler';

const URL_BASE = UrlAssembler('https://circleci.com/api/v1');

export default Map({
  USER_INFO: URL_BASE.segment('/me'),
  ALL_PROJECTS: URL_BASE.segment('/projects'),
  RECENT_BUILDS: URL_BASE.segment('/recent-builds'),
  PROJECT_RECENTS: URL_BASE.template('/project/:user/:repo')
});
