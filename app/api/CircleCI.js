import endpoints from './endpoints';
import request from './request';

function JSONify(response) {
  return response.json();
}

const [GET] = ['GET'];

export async function checkToken(possibleToken) {
  const url = endpoints.get('USER_INFO');
  const response = await request(url, GET, {}, possibleToken);
  return response.ok;
}

export async function getUserDetails() {
  const url = endpoints.get('USER_INFO');
  return await request(url, GET).then(JSONify);
}

export async function getProjects() {
  const url = endpoints.get('ALL_PROJECTS');
  return await request(url, GET).then(JSONify);
}

export async function getProjectRecentBuilds(user, repo, limit = 1, offset = 0) {
  const url = endpoints.get('PROJECT_RECENTS').param({ user, repo }).query({ limit, offset });
  return await request(url, GET).then(JSONify);
}
