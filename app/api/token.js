import { AsyncStorage } from 'react-native';
import settings from '../settings/constants';

async function get() {
  return await AsyncStorage.getItem(settings.get('STORAGE_KEY'));
}

async function set(token) {
  return await AsyncStorage.setItem(settings.get('STORAGE_KEY'), token);
}

async function clear() {
  return await AsyncStorage.removeItem(settings.get('STORAGE_KEY'));
}

export default { get, set, clear };
