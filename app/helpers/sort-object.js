import _ from 'underscore';

export default function sortObject(obj, comparator) {
  let keys = _.sortBy(_.keys(obj), function (key) {
    return comparator ? comparator(obj[key], key) : key;
  });
  return _.object(keys, _.map(keys, function (key) {
    return obj[key];
  }));
}
