import React from 'react-native';
import loaderHandler from 'react-native-busy-indicator/LoaderHandler';
import ProjectList from '../projects/ProjectList';
import GlobalStyles from '../../settings/styles';


const {
  StyleSheet,
  View,
  BackAndroid,
} = React;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.get('CIRCLE_BG'),
  },
});

export default class Home extends React.Component {
  constructor() {
    super();
    this.didReset = false;
  }

  componentWillUnmount() {
    BackAndroid.removeEventListener('hardwareBackPress', () => true);
  }

  componentWillMount() {
    BackAndroid.addEventListener('hardwareBackPress', () => true);
    loaderHandler.showLoader('Fetching');
  }

  componentDidMount() {
    this.props.nav.navigationContext.addListener('didfocus', function () {
      if (!this.didReset) {
        this.props.nav.immediatelyResetRouteStack([this.props.currentRoute]);
        this.didReset = true;
      }
    }.bind(this));
  }

  render() {
    return (
      <View style={styles.container}>
        <ProjectList nav={this.props.nav} />
      </View>
    );
  }
};
