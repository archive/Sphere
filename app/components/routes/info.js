import React from 'react-native';
import GlobalStyles from '../../settings/styles';

const {
  StyleSheet,
  Text,
  View,
  ScrollView,
  BackAndroid,
} = React;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.get('CIRCLE_BG'),
  },
  contentWrapper: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10
  },
  text: {
    color: GlobalStyles.get('CIRCLE_TEXT')
  }
});

export default class Login extends React.Component {

  componentWillUnmount() {
    BackAndroid.removeEventListener('hardwareBackPress', this.props.nav.pop);
  }

  componentWillMount() {
    BackAndroid.addEventListener('hardwareBackPress', this.props.nav.pop);
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView keyboardShouldPersistTaps={false} contentContainerStyle={styles.container}>
            <View style={styles.contentWrapper}>
              <Text style={styles.text}>Content</Text>
            </View>
        </ScrollView>
      </View>
    );
  }
};
