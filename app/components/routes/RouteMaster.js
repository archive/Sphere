import { Map } from 'immutable';

import Login from './login';
import Info from './info';
import Home from './home';
import ProjectDetails from './project-details';

export default Map({
  'LOGIN': {
    id: 'login',
    component: Login,
    hideNavbar: true
  },
  'INFO': {
    id: 'info',
    component: Info
  },
  'HOME': {
    id: 'home',
    component: Home,
    title: 'All Projects'
  },
  'PROJECT_DETAILS': {
    id: 'project_details',
    component: ProjectDetails
  }
});
