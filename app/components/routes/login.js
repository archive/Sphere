import React from 'react-native';
import GlobalStyles from '../../settings/styles';
import TokenHelp from '../login/token-help';
import RouteMaster from './RouteMaster';
import { checkToken } from '../../api/CircleCI';
import token from '../../api/token';
import loaderHandler from 'react-native-busy-indicator/LoaderHandler';

const dismissKeyboard = require('dismissKeyboard');

const {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  ScrollView,
  BackAndroid,
  TouchableWithoutFeedback,
  Alert,
  Image,
  Animated
} = React;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.get('CIRCLE_NAVBAR_BG'),
  },
  contentWrapper: {
    flex: 0.85,
    justifyContent: 'center',
    alignItems: 'center'
  },
  input: {
    fontSize: 14,
    marginHorizontal: 25,
  },
  loginButton: {
    flex: 0.92,
    backgroundColor: GlobalStyles.get('CIRCLE_GREEN'),
    borderColor: GlobalStyles.get('CIRCLE_GREEN'),
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderWidth: 2,
    borderRadius: 12,
    margin: 5
  },
  buttonText: {
    fontSize: 18,
    color: '#FFF'
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 5
  },
  heading: {
    fontSize: 16,
  },
  helpText: {
    fontSize: 12,
    fontStyle: 'italic',
    margin: 5,
    padding: 5,
  },
  text: {
    color: GlobalStyles.get('CIRCLE_NAVBAR_TEXT'),
  },
  icon: {
    height: 75,
    marginBottom: 45
  }
});

export default class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      token: '',
      showTokenHelp: false
    };
    this.animate = new Animated.Value(2);
    this.viewAnimate = {
      transform: [   // Array order matters
        {scale: this.animate.interpolate({
          inputRange: [0, 1],
          outputRange: [1, 0.4],
        })},
        {translateY: this.animate.interpolate({
          inputRange: [0, 1],
          outputRange: [0, -100],
        })},
      ]
    };
    this.validate = this.validate.bind(this);
    this.textChanged = this.textChanged.bind(this);
    this.showTokenHelp = this.showTokenHelp.bind(this);
    this.hideTokenHelp = this.hideTokenHelp.bind(this);
    this.showAbout = this.showAbout.bind(this);
    this.proceed = this.proceed.bind(this);
  }

  textChanged(CIToken) {
    this.setState({ token: CIToken });
  }

  proceed() {
    this.hideTokenHelp();
    this.props.nav.replace(RouteMaster.get('HOME'));
  }

  invalidToken() {
    Alert.alert('Invalid',
      'Token not accepted!',
      [ {text: 'Return' }]);
  }

  validate() {
    loaderHandler.showLoader('Loading');
    dismissKeyboard();
    if (!this.state.token) {
      this.invalidToken();
      loaderHandler.hideLoader();
      return;
    }
    checkToken(this.state.token).then(function (isValid) {
      if (isValid) {
        token.set(this.state.token).then(this.proceed);
      } else {
        this.invalidToken();
        this.setState({
          token: ''
        });
      }
      loaderHandler.hideLoader();
    }.bind(this)).catch(console.log);
  }

  showAbout() {
    this.props.nav.push(RouteMaster.get('INFO'));
  }

  showTokenHelp() {
    dismissKeyboard();
    this.setState({
      showTokenHelp: true
    });
  }

  hideTokenHelp() {
    this.setState({
      showTokenHelp: false
    });
  }

  componentWillUnmount() {
    BackAndroid.removeEventListener('hardwareBackPress', () => { return true; });  // Re-enable back button
  }

  componentWillMount() {
    BackAndroid.addEventListener('hardwareBackPress', () => { return true; });  // Disable back button
  }

  componentDidMount() {
    Animated.spring(this.animate, {
      toValue: 0,
      tension: 30,
    }).start();
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView keyboardShouldPersistTaps={false} contentContainerStyle={styles.container}>
            <View style={styles.contentWrapper}>
              <Animated.Image
                style={[styles.icon, this.viewAnimate]}
                source={require('../../img/circle-ci-logo@light.png')}
                resizeMode="contain"/>
              <Text style={[styles.heading, styles.text]}>
                Please enter your CircleCI token
              </Text>
              <TextInput
                style={[styles.input, styles.text]}
                onChangeText={this.textChanged}
                value={this.state.token}
                onSubmitEditing={this.validate}
                placeholder="CircleCI Token"
                placeholderTextColor="#A7A7A7"
                autoCorrect={false}
              />
              <TouchableWithoutFeedback onPress={this.showTokenHelp}>
                <Text style={[styles.helpText, styles.text]}>
                  What's this?
                </Text>
              </TouchableWithoutFeedback>
            </View>
        </ScrollView>
        <View style={styles.buttonContainer}>
          <TouchableHighlight
            style={[styles.button, styles.loginButton]}
            onPress={this.validate}
            underlayColor={GlobalStyles.get('CIRCLE_BG')}>
            <Text style={[styles.text, styles.buttonText]}>
              Authenticate
            </Text>
          </TouchableHighlight>
        </View>
        <TokenHelp open={this.state.showTokenHelp} onClose={this.hideTokenHelp} />
      </View>
    );
  }
};
