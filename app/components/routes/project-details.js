import React from 'react-native';
import loaderHandler from 'react-native-busy-indicator/LoaderHandler';
import BuildList from '../builds/BuildList';
import GlobalStyles from '../../settings/styles';

const {
  StyleSheet,
  View,
  Text,
} = React;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GlobalStyles.get('CIRCLE_BG')
  },
  title: {
    fontSize: GlobalStyles.get('TITLE_FONT_SIZE'),
    textAlign: 'center'
  },
  statusHeading: {
    fontSize: 18,
    textAlign: 'center'
  },
  statusRow: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
  },
  panel: {
    alignItems: 'stretch',
    borderBottomColor: GlobalStyles.get('CIRCLE_NAVBAR_TEXT'),
    borderBottomWidth: 2,
    paddingVertical: 10
  }
});

export default class ProjectDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      project: props.currentRoute.props.project,
      recentBuilds: false
    };
  }

  componentWillMount() {
    loaderHandler.showLoader('Fetching');
  }


  render() {
    const master = this.state.project.branches[this.state.project.default_branch].recent_builds[0];
    const statusStyle = { color: GlobalStyles.get('CIRCLE_TEST_COLOURS').get(master.outcome.toUpperCase()) };
    return (
      <View style={styles.container}>
        <View style={styles.panel}>
          <Text style={styles.title}>Recent Builds</Text>
          <View style={styles.statusRow}>
            <Text style={styles.statusHeading}>Status: </Text>
            <Text style={[styles.statusHeading, statusStyle]}>{master.outcome}</Text>
          </View>
        </View>
        <BuildList builds={this.state.recentBuilds} project={this.state.project}/>
      </View>
    );
  }
};
