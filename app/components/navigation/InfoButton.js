import React from 'react-native';
import GlobalStyles from '../../settings/styles';
import RouteMaster from '../routes/RouteMaster';

var {
  StyleSheet,
  TouchableHighlight,
  View,
  Text
} = React;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  toolbarButton: {
    paddingHorizontal: 15,
  },
  text: {
    color: GlobalStyles.get('CIRCLE_NAVBAR_TEXT'),
    fontSize: 18,
    fontStyle: 'italic'
  }
});

export default class InfoButton extends React.Component {
  showInfo() {
    if (this.props.route.id !== RouteMaster.get('INFO').id) {
      this.props.nav.push(RouteMaster.get('INFO'));
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <TouchableHighlight
          style={styles.toolbarButton}
          underlayColor={GlobalStyles.get('CIRCLE_NAVBAR_BG')}
          onPress={this.showInfo.bind(this)}>
          <Text style={styles.text}>i</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
