import React from 'react-native';
import GlobalStyles from '../../settings/styles';
import Icon from 'react-native-vector-icons/Ionicons';

var {
  StyleSheet,
  TouchableHighlight,
  View,
  Text
} = React;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  toolbarButton: {
    paddingHorizontal: 15,
  },
  text: {
    color: GlobalStyles.get('CIRCLE_NAVBAR_TEXT'),
    paddingHorizontal: 10
  }
});

export default class NavigationButton extends React.Component {
  goBack() {
    this.props.nav.pop();
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableHighlight
          style={styles.toolbarButton}
          underlayColor={GlobalStyles.get('CIRCLE_NAVBAR_BG')}
          onPress={this.goBack.bind(this)}>
          <Text style={styles.text}><Icon name="chevron-left" /></Text>
        </TouchableHighlight>
      </View>
    );
  }
}
