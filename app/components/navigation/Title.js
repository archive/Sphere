import React from 'react-native';
import GlobalStyles from '../../settings/styles';

const {
  StyleSheet,
  Text,
  View,
} = React;

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    color: GlobalStyles.get('CIRCLE_NAVBAR_TEXT')
  }
});

export default class NavigationTitle extends React.Component {
  render() {
    const title = this.props.route.title || 'Sphere';
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{ title }</Text>
      </View>
    );
  }
}
