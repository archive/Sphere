import { Navigator } from 'react-native';
import _ from 'underscore';

export default class NavigationBar extends Navigator.NavigationBar {
  render () {
    const routes = this.props.navState.routeStack;

    if (routes.length && _.last(routes).hideNavbar) {
      return null;
    }

    return super.render();
  }
}
