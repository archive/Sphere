import React from 'react-native';
import GlobalStyles from '../../settings/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import token from '../../api/token';
import RouteMaster from '../routes/RouteMaster';

const {
  StyleSheet,
  TouchableHighlight,
  Alert,
  View,
  Text
} = React;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  toolbarButton: {
    paddingHorizontal: 15,
  },
  text: {
    color: GlobalStyles.get('CIRCLE_NAVBAR_TEXT'),
    fontSize: 18,
    paddingHorizontal: 10
  }
});

export default class LogoutButton extends React.Component {
  logout() {
    token.clear();
    this.props.nav.resetTo(RouteMaster.get('LOGIN'));
  }
  promptLogout() {
    Alert.alert(
      'Log out?',
      'Are you sure you want to log out?',
      [
        { text: 'Yes', onPress: this.logout.bind(this) },
        { text: 'No' }
      ]
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableHighlight
          style={styles.toolbarButton}
          underlayColor={GlobalStyles.get('CIRCLE_NAVBAR_BG')}
          onPress={this.promptLogout.bind(this)}>
          <Text style={styles.text}><Icon name="sign-out" style={styles.text}/></Text>
        </TouchableHighlight>
      </View>
    );
  }
}
