import React from 'react-native'; // eslint-disable-line no-unused-vars

import Title from './Title';
import BackButton from './BackButton';
import InfoButton from './InfoButton';
import LogoutButton from './LogoutButton';
import RouteMaster from '../routes/RouteMaster';

import { View } from 'react-native';

export default {

  LeftButton(route, nav, index, navState) {
    if (route.id === RouteMaster.get('HOME').id) {
      return <LogoutButton nav={nav}/>;
    }
    if (index >= 1) {
      return (
        <BackButton nav={nav} />
      );
    }
    return <View />;
  },

  RightButton(route, nav, index, navState) {
    if (route.id === RouteMaster.get('LOGIN').id) {
      return <InfoButton nav={nav} route={route} />;
    }
    return <View />;
  },

  Title(route, nav, index, navState) {
    return (
      <Title route={route} />
    );
  },

};
