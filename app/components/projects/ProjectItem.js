import React from 'react-native';
import GlobalStyles from '../../settings/styles';
import { getProjectRecentBuilds } from '../../api/CircleCI';
import moment from 'moment';
import Icon from 'react-native-vector-icons/Octicons';
import RouteMaster from '../routes/RouteMaster';

const {
  View,
  Text,
  StyleSheet,
  TouchableHighlight
} = React;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 70,
    alignItems: 'stretch',
    backgroundColor: GlobalStyles.get('CIRCLE_ITEM_BG'),
    borderBottomColor: GlobalStyles.get('CIRCLE_ITEM_BORDER'),
    borderBottomWidth: 2,
    flexDirection: 'row',
  },
  button: {
    marginVertical: 2.5,
  },
  repoCell: {
    paddingHorizontal: 4,
    paddingVertical: 3,
    flex: 0.84
  },
  buildCell: {
    flex: 0.25,
    padding: 4,
    borderLeftColor: GlobalStyles.get('CIRCLE_ITEM_BORDER'),
    borderLeftWidth: 1
  },
  colourPanel: {
    flex: 0.02
  },
  repoName: {
    fontSize: 15,
    fontWeight: '500'
  },
  details: {
    fontSize: 12.5,
    fontWeight: '300',
  },
  detailsRow: {
    flex: 1,
    flexDirection: 'row'
  },
  mainDetails: {
    marginRight: 10
  }
});

export default class ProjectItem extends React.Component {
  constructor() {
    super();
    this.state = {
      recentBuild: false
    };
    this._displayMoreInfo = this._displayMoreInfo.bind(this);
    this._viewBuildDetails = this._viewBuildDetails.bind(this);
  }

  componentDidMount() {
    getProjectRecentBuilds(this.props.project.username, this.props.project.reponame).then(function (recentBuild) {
      this.setState({ recentBuild: recentBuild[0] });
    }.bind(this));
  }

  _viewBuildDetails() {
    const destination = RouteMaster.get('PROJECT_DETAILS');
    destination.title = this.props.project.reponame;
    destination.props = {
      project: this.props.project
    };
    this.props.nav.push(destination);
  }

  _displayMoreInfo(mostRecentBuild) {
    const mask = mostRecentBuild.build_time_millis <= 60000 ? 'ss[s]' : 'm:ss';
    const buildTime = moment.duration(mostRecentBuild.build_time_millis, 'ms').format(mask);
    const commit = mostRecentBuild.all_commit_details[0];
    return (
      <View style={styles.buildCell}>
        <Text style={styles.details}><Icon name="pulse" /> #{ mostRecentBuild.build_num }</Text>
        <Text style={styles.details}><Icon name="clock" /> { buildTime }</Text>
        <Text style={styles.details}><Icon name="git-commit" /> { commit.commit.substring(0, 6) }</Text>
      </View>
    );
  }

  render() {
    const project = this.props.project;
    const mostRecentBuild = this.state.recentBuild;
    const master = project.branches[project.default_branch].recent_builds[0];

    const statusColour = GlobalStyles.get('CIRCLE_TEST_COLOURS').get(master.outcome.toUpperCase());
    const username = this.props.userDetails && this.props.userDetails.login !== project.username ?
      (<Text style={[styles.details, styles.mainDetails]}><Icon name="person" /> {project.username}</Text>) : null;

    const language = project.language ?
      (<Text style={[styles.details, styles.mainDetails]}><Icon name="keyboard" /> {project.language}</Text>) : null;

    const buildDetails = mostRecentBuild ? this._displayMoreInfo(mostRecentBuild) : null;

    return (
      <TouchableHighlight
        style={styles.button}
        underlayColor={GlobalStyles.get('CIRCLE_BG')}
        onPress={this._viewBuildDetails}>
        <View style={styles.container}>
          <View style={styles.repoCell}>
            <Text style={styles.repoName}>{project.reponame}</Text>
            <View style={styles.detailsRow}>
              { username }
              { language }
            </View>
          </View>
          { buildDetails }
          <View style={[styles.colourPanel, {backgroundColor: statusColour}]} />
        </View>
      </TouchableHighlight>
    );
  }
};
