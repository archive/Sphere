import React from 'react-native';
import GiftedListView from 'react-native-gifted-listview';
import ProjectItem from './ProjectItem';
import { getProjects, getUserDetails } from '../../api/CircleCI';
import loaderHandler from 'react-native-busy-indicator/LoaderHandler';
import _ from 'underscore';
import sortObject from '../../helpers/sort-object';

const {
  StyleSheet,
  View
} = React;

const styles = StyleSheet.create({
  listView: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
    flexWrap: 'wrap',
    paddingHorizontal: 5
  },
});

export default class ProjectList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userDetails: false,
    };
    this.renderRow = this.renderRow.bind(this);
    this._fetch = this._fetch.bind(this);
  }

  componentWillMount() {
    getUserDetails().then(function(userDetails) {
      this.setState({ userDetails });
    }.bind(this));
  }

  _sortByDate(projects) {
    return _.sortBy(projects, function (project) {
      let mostRecentBranch = sortObject(project.branches, function (branch) {
        return branch.recent_builds[0].pushed_at;
      });
      mostRecentBranch = mostRecentBranch[Object.keys(mostRecentBranch)[0]];
      return mostRecentBranch.recent_builds[0].pushed_at;
    }).reverse();
  }

  renderRow(project) {
    return <ProjectItem project={project} userDetails={this.state.userDetails} nav={this.props.nav} />;
  }


  _fetch(page, callback, options) {
    getProjects().then((projects) => {
      callback(this._sortByDate(projects));
      loaderHandler.hideLoader();
    });
  }

  render() {
    if (!this.state.userDetails) {
      return <View />;
    }
    return (
      <View style={styles.listView}>
        <GiftedListView
          rowView={this.renderRow}
          refreshable={true}
          pagination={false}
          onFetch={this._fetch} />
      </View>
    );
  }
};
