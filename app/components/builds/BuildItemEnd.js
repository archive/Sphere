import React from 'react-native';
import GlobalStyles from '../../settings/styles';

const {
  View,
  Text,
  StyleSheet
} = React;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: GlobalStyles.get('CIRCLE_ITEM_BG'),
    borderBottomColor: GlobalStyles.get('CIRCLE_ITEM_BORDER'),
    borderBottomWidth: 2,
    flexDirection: 'row',
  },
  text: {
    fontSize: 18,
  }
});

export default class BuildItem extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>That's all!</Text>
      </View>
    );
  }
};
