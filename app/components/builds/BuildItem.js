import React from 'react-native';
import GlobalStyles from '../../settings/styles';
import Icon from 'react-native-vector-icons/Octicons';
import moment from 'moment';
import { timeSince } from '../../helpers/time-utils';

const {
  View,
  Text,
  StyleSheet,
  TouchableHighlight
} = React;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 70,
    alignItems: 'stretch',
    backgroundColor: GlobalStyles.get('CIRCLE_ITEM_BG'),
    borderBottomColor: GlobalStyles.get('CIRCLE_ITEM_BORDER'),
    borderBottomWidth: 2,
    flexDirection: 'row',
  },
  button: {
    marginVertical: 2.5,
  },
  repoCell: {
    paddingHorizontal: 4,
    paddingVertical: 3,
    flex: 0.84
  },
  buildCell: {
    flex: 0.25,
    padding: 4,
    borderLeftColor: GlobalStyles.get('CIRCLE_ITEM_BORDER'),
    borderLeftWidth: 1
  },
  colourPanel: {
    flex: 0.02
  },
  repoName: {
    fontSize: 15,
    fontWeight: '500'
  },
  details: {
    fontSize: 12.5,
    fontWeight: '300',
  },
  detailsRow: {
    flex: 1,
    flexDirection: 'row'
  }
});

export default class BuildItem extends React.Component {
  constructor(props) {
    super(props);
    this._displayMoreInfo = this._displayMoreInfo.bind(this);
  }

  _displayMoreInfo(build) {
    const mask = build.build_time_millis <= 60000 ? 'ss[s]' : 'm:ss';
    const buildTime = moment.duration(build.build_time_millis, 'ms').format(mask);
    const commit = build.all_commit_details[0];
    return (
      <View style={styles.buildCell}>
        <Text style={styles.details}>
          <Icon name="info" /> { build.status }
        </Text>
        <Text style={styles.details}><Icon name="clock" /> { buildTime }</Text>
        <Text style={styles.details}><Icon name="git-commit" /> { commit.commit.substring(0, 6) }</Text>
      </View>
    );
  }

  render() {
    const build = this.props.build;
    const buildDetails = this._displayMoreInfo(build);
    return (
      <TouchableHighlight
        style={styles.button}
        underlayColor={GlobalStyles.get('CIRCLE_BG')}>
        <View style={styles.container}>
          <View style={styles.repoCell}>
            <Text style={styles.repoName}>{build.branch} - #{build.build_num}</Text>
            <View style={styles.detailsRow}>
              <Text style={styles.details}>
                <Icon name="clock" /> {timeSince(moment(build.stop_time).toDate())} ago
              </Text>
            </View>
            <View style={styles.detailsRow}>
              <Text style={styles.details}><Icon name="comment-discussion" /> { build.subject }</Text>
            </View>
          </View>
          { buildDetails }
          <View style={[styles.colourPanel, {backgroundColor: this.props.statusColour}]} />
        </View>
      </TouchableHighlight>
    );
  }
};
