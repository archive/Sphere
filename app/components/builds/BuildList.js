import React from 'react-native';
import GiftedListView from 'react-native-gifted-listview';
import GlobalStyles from '../../settings/styles';
import loaderHandler from 'react-native-busy-indicator/LoaderHandler';
import BuildItem from './BuildItem';
import BuildItemEnd from './BuildItemEnd';
import { getProjectRecentBuilds } from '../../api/CircleCI';
import _ from 'underscore';

const {
  StyleSheet,
  View
} = React;

const styles = StyleSheet.create({
  listView: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
    flexWrap: 'wrap',
    paddingHorizontal: 5
  },
});

export default class BuildList extends React.Component {
  constructor(props) {
    super(props);
    this._fetch = this._fetch.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.renderLimit = 20;
  }

  renderRow(build) {
    if (build.build_time_millis < 1) {
      return null;
    }
    const statusColour = GlobalStyles.get('CIRCLE_TEST_COLOURS').get(build.status.toUpperCase());
    return <BuildItem build={build} project={this.props.project} statusColour={statusColour} />;
  }

  _fetch(page = 1, callback, options) {
    const project = this.props.project;
    getProjectRecentBuilds(project.username, project.reponame, this.renderLimit, this.renderLimit * (page - 1))
    .then((recentBuilds) => {
      recentBuilds = _.sortBy(recentBuilds, (b) => b.stop_time).reverse();
      loaderHandler.hideLoader();
      callback(recentBuilds, {
        allLoaded: recentBuilds.length < this.renderLimit
      });
    });
  }

  render() {
    return (
      <View style={styles.listView}>
        <GiftedListView
          rowView={this.renderRow}
          refreshable={true}
          onFetch={this._fetch}
          paginationAllLoadedView={() => <BuildItemEnd />}/>
      </View>
    );
  }
};
