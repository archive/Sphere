import React from 'react-native';
import GlobalStyles from '../../settings/styles';
import Modal from 'react-native-modalbox';

const {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback
} = React;

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: GlobalStyles.get('CIRCLE_BG'),
    height: 200
  },
  title: {
    fontSize: 18,
    color: GlobalStyles.get('CIRCLE_TEXT'),
  },
  text: {
    fontSize: 13,
    color: GlobalStyles.get('CIRCLE_TEXT'),
    margin: 2,
    textAlign: 'center'
  },
  dismiss: {
    fontSize: 12,
    padding: 5
  },
  modalView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10
  }
});

export default class Login extends React.Component {
  hide() {
    this.refs.modal.close();
  }
  render() {
    return (
      <Modal
        isOpen={this.props.open}
        style={styles.modal}
        ref="modal"
        onClosed={this.props.onClose}
        backdrop={true}>
        <View style={styles.modalView}>
          <Text style={styles.title}>
            What is a token?
          </Text>
          <Text style={styles.text}>
            A token is the identifier used to authenticate with CircleCI and let Sphere interact your account.
          </Text>
          <Text style={styles.text}>
            To generate a new token, visit your CircleCI accounts page.
          </Text>
        </View>
        <TouchableWithoutFeedback onPress={this.hide.bind(this)}>
          <Text style={[styles.text, styles.dismiss]}>
            Swipe down to dismiss
          </Text>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
};
